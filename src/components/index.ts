export * from './partials/Footer'
export * from './partials/Header'
export * from './partials/InfosContact'
export * from './sections/Experience'
export * from './sections/Expertise'
export * from './sections/Presentation'
export * from './partials/Parallax'
export * from './commons/SlideShowProject'



