export function loadImage(imagePath: string) {
    try {
        return require(`../${imagePath}`);
    } catch (err) {
        console.error(`Impossible to load the image ${imagePath}`, err);
        return null;
    }
}
